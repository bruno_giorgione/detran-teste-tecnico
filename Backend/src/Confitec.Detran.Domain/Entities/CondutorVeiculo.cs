﻿using Confitec.Detran.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Entities
{
    public class CondutorVeiculo : EntityBase, IAggregateRoot
    {
        public Guid CondutorId { get; set; }
        public Guid VeiculoId { get; set; }

        /*EF Navigation*/
        public Condutor Condutor { get; set; }
        public Veiculo Veiculo { get; set; }

        /*EF migration*/
        public CondutorVeiculo() { }
    }
}
