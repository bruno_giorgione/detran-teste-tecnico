﻿using Confitec.Detran.Domain.Interfaces;
using Confitec.Detran.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;

namespace Confitec.Detran.Domain.Entities
{
    public class Condutor : EntityBase, IAggregateRoot
    {
        public string Nome { get; private set; }
        public string CPF { get; private set; }
        public string Telefone { get; private set; }
        public string Email { get; private set; }
        public string NumeroCNH { get; private set; }
        public DateTime DataNascimento { get; private set; }
        public DateTime DataCadastro { get; private set; }

        /*EF Navigation*/
        public IList<CondutorVeiculo> CondutorVeiculos { get; set; }

        /*EF migration*/
        public Condutor() { }

        public Condutor(string nome, string cpf, string telefone,
                        string email, string numeroCNH,
                        DateTime dataNascimento)
        {
            Nome = nome;
            CPF = cpf;
            Telefone = telefone;
            Email = email;
            NumeroCNH = numeroCNH;
            DataNascimento = dataNascimento;
        }
    }
}
