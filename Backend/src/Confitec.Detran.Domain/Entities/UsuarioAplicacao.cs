﻿using Confitec.Detran.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Entities
{
    public class UsuarioAplicacao : EntityBase, IAggregateRoot
    {
        public string Email { get; private set; }
        public string Senha { get; private set; }

        public string Nome { get; private set; }

        public PermissaoTipo PermissaoTipo { get; private set; }

        public UsuarioAplicacao(string email, string senha, string nome)
        {
            Email = email;
            Senha = senha;
            Nome = nome;
        }

        public void ApplicarPermissao(PermissaoTipo permissao)
        {
            PermissaoTipo = permissao;
        }
    }

    public enum PermissaoTipo
    {
        ADMIN = 1,
        CONSULTOR = 2
    }
}
