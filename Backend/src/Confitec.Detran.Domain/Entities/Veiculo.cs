﻿using Confitec.Detran.Domain.Interfaces;
using Confitec.Detran.Domain.Interfaces.Entities;
using System;
using System.Collections.Generic;

namespace Confitec.Detran.Domain.Entities
{
    public class Veiculo : EntityBase, IAggregateRoot
    {
        public string Placa { get; private set; }
        public string Modelo { get; private set; }
        public string Marca { get; private set; }
        public string Cor { get; private set; }
        public string AnoFabricacao { get; private set; }
        public DateTime DataCadastro { get; private set; }

        /*EF Navigation*/
        public IList<CondutorVeiculo> CondutorVeiculos { get; set; }

        /*EF migration*/
        public Veiculo() { }
        public Veiculo(string placa, string modelo, string marca, string cor, string anoFabricacao)
        {
            Placa = placa;
            Modelo = modelo;
            Marca = marca;
            Cor = cor;
            AnoFabricacao = anoFabricacao;
        }
    }
}
