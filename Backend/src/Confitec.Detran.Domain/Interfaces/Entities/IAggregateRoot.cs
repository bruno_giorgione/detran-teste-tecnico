﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Interfaces.Entities
{
    /// <summary>
    /// Interface de marcação apenas para informar qual entidade é a raíz de agregação 
    /// </summary>
    public interface IAggregateRoot {}
}
