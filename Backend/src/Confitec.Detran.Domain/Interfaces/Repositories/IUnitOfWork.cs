﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Interfaces.Repositories
{
    public interface IUnitOfWork
    {
        bool Commit();
    }
}
