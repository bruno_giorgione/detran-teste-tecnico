﻿using Confitec.Detran.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Interfaces.Repositories
{
    public interface IUsuarioAplicacaoRepository : IRepository<UsuarioAplicacao>
    {
        UsuarioAplicacao Autenticar(string email, string senha);
    }
}
