﻿using Confitec.Detran.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Interfaces.Repositories
{
    public interface IVeiculoRepository : IRepository<Veiculo>
    {
        IList<Veiculo> ObterTodos();
        Veiculo ObterPorId(Guid id);
        IList<Condutor> ObterCondutoresPorPlaca(string placa);
        void Adicionar(Veiculo veiculo);
        void Atualizar(Veiculo veiculo);
        void Remover(Guid id);
    }
}
