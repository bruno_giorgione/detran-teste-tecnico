﻿using Confitec.Detran.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Interfaces.Repositories
{
    public interface ICondutorRepository : IRepository<Condutor>
    {
        IList<Condutor> ObterTodos();
        Condutor ObterPorId(Guid id);
        IList<Veiculo> ObterVeiculosPorCpf(string cpf);
        void Adicionar(Condutor condutor);
        void Atualizar(Condutor condutor);
        void Remover(Guid id);
        bool VerificarCpfJaExiste(string cpf);
    }
}
