﻿using Confitec.Detran.Domain.Interfaces.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Domain.Services
{
    public class CondutorService
    {
        private readonly ICondutorRepository _repository;

        public CondutorService(ICondutorRepository repository)
        {
            _repository = repository;
        }

        public bool VerificarCpfJaExiste(string cpf)
        {
            return _repository.VerificarCpfJaExiste(cpf);
        }
    }
}
