﻿using Confitec.Detran.Services.API;
using Confitec.Detran.Services.API.Dtos;
using Confitec.Detran.WebApi.Tests.Integrations.Configurations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Confitec.Detran.WebApi.Tests.Integrations
{
    public class UsuarioAplicacaoTests
    {
        private readonly ConfitecDetranFactory<StartupTests> _webFactory;
        private HttpClient _httpClient;
        public UsuarioAplicacaoTests()
        {
            _webFactory = new ConfitecDetranFactory<StartupTests>();
            _httpClient = _webFactory.CreateClient();
        }

        [Fact(DisplayName = "Efetuar Login com sucesso")]
        [Trait("Categoria", "Usuário Api Testes - Integração")]
        public async Task RealizarLogin_Login_DeveRetornarToken()
        {
            //Arrange
            var usuarioAutenticado = new UsuarioAplicacaoAutenticadoDto();
            var bodyContent = new
            {
                Email = "admin@confitec.com.br",
                Senha = "123"
            };
            var httpContent = JsonContent.Create(bodyContent);

            //Act
            var response = await _httpClient.PostAsync("api/autenticacao/login", httpContent);
            if (response.IsSuccessStatusCode)
            {
                usuarioAutenticado = JsonConvert.DeserializeObject<UsuarioAplicacaoAutenticadoDto>(await response.Content.ReadAsStringAsync());
            }
            //Assert
            Assert.True(usuarioAutenticado.Token != null);
        }
    }
   
   
}
