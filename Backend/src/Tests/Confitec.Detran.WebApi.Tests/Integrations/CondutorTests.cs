﻿using Confitec.Detran.Services.API;
using Confitec.Detran.Services.API.Dtos;
using Confitec.Detran.WebApi.Tests.Integrations.Configurations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xunit;

namespace Confitec.Detran.WebApi.Tests.Integrations
{
    public class CondutorTests
    {
        private readonly ConfitecDetranFactory<StartupTests> _webFactory;
        private HttpClient _httpClient;
        public CondutorTests()
        {
            _webFactory = new ConfitecDetranFactory<StartupTests>();
            _httpClient = _webFactory.CreateClient();
        }

        [Fact(DisplayName = "Cadastrar novo condutor com sucesso")]
        [Trait("Categoria", "Condutor Api Testes - Integração")]
        public async Task CadastrarCondutor_NovoCondutor_DeveRetornarSucesso()
        {
            

            Assert.True(1 == 1);
            
        }

        [Fact(DisplayName = "Atualizar condutor com sucesso")]
        [Trait("Categoria", "Condutor Api Testes - Integração")]
        public async Task AtualizarCondutor_AtualizarCondutor_DeveRetornarSucesso()
        {
           //TODO
        }
    }
}
