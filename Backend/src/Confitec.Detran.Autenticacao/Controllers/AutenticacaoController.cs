﻿using AutoMapper.Configuration;
using Confitec.Detran.Services.API.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Controllers
{
    [Route("api/autenticacao")]
    [ApiController]
    public class AutenticacaoController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public AutenticacaoController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpPost("login")]
        public IActionResult Autenticar(UsuarioAplicacaoDto usuarioAplicacaoDto)
        {
            if (!usuarioAplicacaoDto.EstaValido()) return BadRequest(usuarioAplicacaoDto.ObterErros());



            if (usuario.Email == "bruno.giorgione@outlook.com" && usuario.Senha == "123")
            {
                return Ok(GerarJWTToken(usuario.Email));
            }

            return BadRequest();
        }
    }
}
