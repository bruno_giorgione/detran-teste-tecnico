﻿using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Contexts
{
    public class DetranContext : DbContext, IUnitOfWork
    {
        public DetranContext(DbContextOptions<DetranContext> options)
              : base(options){}

        public DbSet<UsuarioAplicacao> UsuariosAplicacao { get; set; }
        public DbSet<Condutor> Condutores { get; set; }
        public DbSet<Veiculo> Veiculos { get; set; }
        public DbSet<CondutorVeiculo> CondutorVeiculos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DetranContext).Assembly);
        }
        public bool Commit()
        {
            return CommitAll();
        }

        /// <summary>
        /// Método para salvar as datas de cadastros de todas as entidades de forma automática.
        /// </summary>
        /// <returns></returns>
        public bool CommitAll()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }
            return base.SaveChanges() > 0;
        }
       
    }
}
