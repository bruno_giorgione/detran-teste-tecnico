﻿using Confitec.Detran.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Mappings
{
    public class CondutorMapping : IEntityTypeConfiguration<Condutor>
    {
        public void Configure(EntityTypeBuilder<Condutor> builder)
        {
            builder.HasKey("Id");

            builder.Property(v => v.CPF)
                .IsRequired()
                .HasColumnType("VARCHAR(11)")
                .HasColumnName("CPF");

            builder.Property(v => v.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("Nome");

            builder.Property(v => v.Email)
                .IsRequired()
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("Email");

            builder.Property(v => v.NumeroCNH)
                .IsRequired()
                .HasColumnType("varchar(11)")
                .HasColumnName("NumeroCNH");

            builder.Property(v => v.Telefone)
                .IsRequired()
                .HasColumnType("varchar(15)")
                .HasColumnName("Telefone");

            builder.Property(v => v.DataNascimento)
                .IsRequired()
                .HasColumnType("DATE")
                .HasColumnName("DataNascimento");

            builder.Property(v => v.DataCadastro)
                .IsRequired()
                .HasColumnType("DATETIME")
                .HasColumnName("DataCadastro");

            builder.ToTable("Condutores");
        }
    }
}
