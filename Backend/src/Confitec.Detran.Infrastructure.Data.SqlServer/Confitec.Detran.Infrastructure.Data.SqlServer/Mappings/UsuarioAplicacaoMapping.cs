﻿using Confitec.Detran.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Mappings
{
    public class UsuarioAplicacaoMapping : IEntityTypeConfiguration<UsuarioAplicacao>
    {
        public void Configure(EntityTypeBuilder<UsuarioAplicacao> builder)
        {
            builder.HasKey("Id");

            builder.Property(v => v.Email)
                .IsRequired()
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("Email");

            builder.Property(v => v.Senha)
                .IsRequired()
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("Senha");

            builder.Property(v => v.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(255)")
                .HasColumnName("Nome");

            builder.Property(v => v.PermissaoTipo)
              .IsRequired()
              .HasColumnType("INT")
              .HasColumnName("PermissaoTipo");

          
            builder.ToTable("UsuariosAplicacao");
        }
    }
}
