﻿using Confitec.Detran.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Mappings
{
    public class CondutorVeiculoMapping : IEntityTypeConfiguration<CondutorVeiculo>
    {
        public void Configure(EntityTypeBuilder<CondutorVeiculo> builder)
        {
            builder.HasKey("Id");

            builder.Property(cv => cv.CondutorId)
                .IsRequired()
                .HasColumnType("uniqueidentifier")
                .HasColumnName("CondutorId");

            builder.Property(cv => cv.VeiculoId)
               .IsRequired()
               .HasColumnType("uniqueidentifier")
               .HasColumnName("VeiculoId");

            //Relacionamento N:N
            builder
               .HasOne(cv => cv.Condutor)
               .WithMany(b => b.CondutorVeiculos);
            builder
               .HasOne(cv => cv.Veiculo)
               .WithMany(b => b.CondutorVeiculos);

            builder.ToTable("CondutorVeiculos");
        }
    }
}
