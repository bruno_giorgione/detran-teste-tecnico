﻿using Confitec.Detran.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Mappings
{
    public class VeiculoMapping : IEntityTypeConfiguration<Veiculo>
    {
        public void Configure(EntityTypeBuilder<Veiculo> builder)
        {
            builder.HasKey("Id");

            builder.Property(v => v.Placa)
                .IsRequired()
                .HasColumnType("VARCHAR(8)")
                .HasColumnName("Placa");

            builder.Property(v => v.Marca)
                .IsRequired()
                .HasColumnType("VARCHAR(250)")
                .HasColumnName("Marca");

            builder.Property(v => v.Modelo)
                .IsRequired()
                .HasColumnType("VARCHAR(300)")
                .HasColumnName("Modelo");

            builder.Property(v => v.Cor)
                .IsRequired()
                .HasColumnType("VARCHAR(150)")
                .HasColumnName("Cor");

            builder.Property(v => v.AnoFabricacao)
                .IsRequired()
                .HasColumnType("varchar(4)")
                .HasColumnName("AnoFabricacao");

            builder.Property(v => v.DataCadastro)
               .IsRequired()
               .HasColumnType("DATETIME")
               .HasColumnName("DataCadastro");

            builder.ToTable("Veiculos");
        }
    }
}
