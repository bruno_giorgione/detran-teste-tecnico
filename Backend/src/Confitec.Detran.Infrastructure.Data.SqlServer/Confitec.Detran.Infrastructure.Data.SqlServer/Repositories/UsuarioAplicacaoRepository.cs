﻿using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Infrastructure.Data.SqlServer.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Repositories
{
    public class UsuarioAplicacaoRepository : IUsuarioAplicacaoRepository
    {
        private readonly DetranContext _context;

        public UsuarioAplicacaoRepository(DetranContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public UsuarioAplicacao Autenticar(string email, string senha)
        {
            return _context.UsuariosAplicacao
                .FirstOrDefault(
                        u => u.Email == email 
                        && u.Senha == senha 
                   );
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
