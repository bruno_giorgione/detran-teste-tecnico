﻿using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Infrastructure.Data.SqlServer.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Repositories
{
    public class CondutorRepository : ICondutorRepository
    {
        private readonly DetranContext _context;

        public CondutorRepository(DetranContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IList<Condutor> ObterTodos()
        {
            return _context.Condutores.AsNoTracking().ToList();
        }
        public Condutor ObterPorId(Guid id)
        {
            return _context.Condutores.AsNoTracking()
                    .FirstOrDefault(c => c.Id == id);
        }
        public IList<Veiculo> ObterVeiculosPorCpf(string cpf)
        {
            return (from v in _context.Veiculos
                    join cv in _context.CondutorVeiculos on v.Id equals cv.VeiculoId
                    join c in _context.Condutores on cv.CondutorId equals c.Id
                    where c.CPF == cpf
                    orderby v.Marca
                    select v).ToList();
        }

        public void Adicionar(Condutor condutor)
        {
            _context.Add(condutor);
        }
        public void Atualizar(Condutor condutor)
        {
            _context.Update(condutor);
        }

        public void Remover(Guid id)
        {
            var model = ObterPorId(id);
            _context.Condutores.Remove(model);
        }

        public bool VerificarCpfJaExiste(string cpf)
        {
            return _context.Condutores.Any(c => c.CPF == cpf);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }

       
    }
}
