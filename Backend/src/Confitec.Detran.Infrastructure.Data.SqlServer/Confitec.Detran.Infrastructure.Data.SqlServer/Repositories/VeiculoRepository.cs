﻿using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Infrastructure.Data.SqlServer.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Repositories
{
    public class VeiculoRepository : IVeiculoRepository
    {
        private readonly DetranContext _context;

        public VeiculoRepository(DetranContext context)
        {
            _context = context;
        }

        public IUnitOfWork UnitOfWork => _context;

        public IList<Veiculo> ObterTodos()
        {
            return  _context.Veiculos.AsNoTracking().ToList();
        }
        public Veiculo ObterPorId(Guid id)
        {
            return _context.Veiculos.AsNoTracking()
                    .FirstOrDefault(c => c.Id == id);
        }
        public IList<Condutor> ObterCondutoresPorPlaca(string placa)
        {
            return (from c in _context.Condutores
                    join cv in _context.CondutorVeiculos on c.Id equals cv.CondutorId
                    join v in _context.Veiculos on cv.VeiculoId equals v.Id
                    where v.Placa == placa
                    orderby c.Nome
                    select c).ToList();
        }

        public void Adicionar(Veiculo veiculo)
        {
            _context.Add(veiculo);
        }
        public void Atualizar(Veiculo veiculo)
        {
            _context.Update(veiculo);
        }

        public void Remover(Guid id)
        {
            var model = ObterPorId(id);
            _context.Veiculos.Remove(model);
        }

        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
