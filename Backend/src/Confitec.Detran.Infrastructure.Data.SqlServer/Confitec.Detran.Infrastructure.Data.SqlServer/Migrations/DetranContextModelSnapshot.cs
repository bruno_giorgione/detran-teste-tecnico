﻿// <auto-generated />
using System;
using Confitec.Detran.Infrastructure.Data.SqlServer.Contexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Migrations
{
    [DbContext(typeof(DetranContext))]
    partial class DetranContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.11")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.Condutor", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("CPF")
                        .IsRequired()
                        .HasColumnType("VARCHAR(11)")
                        .HasColumnName("CPF");

                    b.Property<DateTime>("DataCadastro")
                        .HasColumnType("DATETIME")
                        .HasColumnName("DataCadastro");

                    b.Property<DateTime>("DataNascimento")
                        .HasColumnType("DATE")
                        .HasColumnName("DataNascimento");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("VARCHAR(255)")
                        .HasColumnName("Email");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("VARCHAR(255)")
                        .HasColumnName("Nome");

                    b.Property<string>("NumeroCNH")
                        .IsRequired()
                        .HasColumnType("varchar(11)")
                        .HasColumnName("NumeroCNH");

                    b.Property<string>("Telefone")
                        .IsRequired()
                        .HasColumnType("varchar(15)")
                        .HasColumnName("Telefone");

                    b.HasKey("Id");

                    b.ToTable("Condutores");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.CondutorVeiculo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("CondutorId")
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("CondutorId");

                    b.Property<Guid>("VeiculoId")
                        .HasColumnType("uniqueidentifier")
                        .HasColumnName("VeiculoId");

                    b.HasKey("Id");

                    b.HasIndex("CondutorId");

                    b.HasIndex("VeiculoId");

                    b.ToTable("CondutorVeiculos");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.UsuarioAplicacao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnType("VARCHAR(255)")
                        .HasColumnName("Email");

                    b.Property<string>("Nome")
                        .IsRequired()
                        .HasColumnType("VARCHAR(255)")
                        .HasColumnName("Nome");

                    b.Property<int>("PermissaoTipo")
                        .HasColumnType("INT")
                        .HasColumnName("PermissaoTipo");

                    b.Property<string>("Senha")
                        .IsRequired()
                        .HasColumnType("VARCHAR(255)")
                        .HasColumnName("Senha");

                    b.HasKey("Id");

                    b.ToTable("UsuariosAplicacao");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.Veiculo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AnoFabricacao")
                        .IsRequired()
                        .HasColumnType("varchar(4)")
                        .HasColumnName("AnoFabricacao");

                    b.Property<string>("Cor")
                        .IsRequired()
                        .HasColumnType("VARCHAR(150)")
                        .HasColumnName("Cor");

                    b.Property<DateTime>("DataCadastro")
                        .HasColumnType("DATETIME")
                        .HasColumnName("DataCadastro");

                    b.Property<string>("Marca")
                        .IsRequired()
                        .HasColumnType("VARCHAR(250)")
                        .HasColumnName("Marca");

                    b.Property<string>("Modelo")
                        .IsRequired()
                        .HasColumnType("VARCHAR(300)")
                        .HasColumnName("Modelo");

                    b.Property<string>("Placa")
                        .IsRequired()
                        .HasColumnType("VARCHAR(8)")
                        .HasColumnName("Placa");

                    b.HasKey("Id");

                    b.ToTable("Veiculos");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.CondutorVeiculo", b =>
                {
                    b.HasOne("Confitec.Detran.Domain.Entities.Condutor", "Condutor")
                        .WithMany("CondutorVeiculos")
                        .HasForeignKey("CondutorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Confitec.Detran.Domain.Entities.Veiculo", "Veiculo")
                        .WithMany("CondutorVeiculos")
                        .HasForeignKey("VeiculoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Condutor");

                    b.Navigation("Veiculo");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.Condutor", b =>
                {
                    b.Navigation("CondutorVeiculos");
                });

            modelBuilder.Entity("Confitec.Detran.Domain.Entities.Veiculo", b =>
                {
                    b.Navigation("CondutorVeiculos");
                });
#pragma warning restore 612, 618
        }
    }
}
