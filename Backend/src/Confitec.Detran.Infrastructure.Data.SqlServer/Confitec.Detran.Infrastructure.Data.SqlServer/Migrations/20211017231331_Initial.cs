﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Confitec.Detran.Infrastructure.Data.SqlServer.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Condutores",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(255)", nullable: false),
                    CPF = table.Column<string>(type: "VARCHAR(11)", nullable: false),
                    Telefone = table.Column<string>(type: "varchar(15)", nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(255)", nullable: false),
                    NumeroCNH = table.Column<string>(type: "varchar(11)", nullable: false),
                    DataNascimento = table.Column<DateTime>(type: "DATE", nullable: false),
                    DataCadastro = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Condutores", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UsuariosAplicacao",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(255)", nullable: false),
                    Senha = table.Column<string>(type: "VARCHAR(255)", nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(255)", nullable: false),
                    PermissaoTipo = table.Column<int>(type: "INT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UsuariosAplicacao", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Veiculos",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Placa = table.Column<string>(type: "VARCHAR(8)", nullable: false),
                    Modelo = table.Column<string>(type: "VARCHAR(300)", nullable: false),
                    Marca = table.Column<string>(type: "VARCHAR(250)", nullable: false),
                    Cor = table.Column<string>(type: "VARCHAR(150)", nullable: false),
                    AnoFabricacao = table.Column<string>(type: "varchar(4)", nullable: false),
                    DataCadastro = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Veiculos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CondutorVeiculos",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CondutorId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    VeiculoId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CondutorVeiculos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CondutorVeiculos_Condutores_CondutorId",
                        column: x => x.CondutorId,
                        principalTable: "Condutores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CondutorVeiculos_Veiculos_VeiculoId",
                        column: x => x.VeiculoId,
                        principalTable: "Veiculos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CondutorVeiculos_CondutorId",
                table: "CondutorVeiculos",
                column: "CondutorId");

            migrationBuilder.CreateIndex(
                name: "IX_CondutorVeiculos_VeiculoId",
                table: "CondutorVeiculos",
                column: "VeiculoId");

            var id = Guid.NewGuid();
            migrationBuilder.InsertData(
               table: "UsuariosAplicacao",
               columns: new[] { "Id", "Email", "Senha", "Nome", "PermissaoTipo" },
               values: new object[] { id, "admin@confitec.com.br", "123", "Confitec", 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CondutorVeiculos");

            migrationBuilder.DropTable(
                name: "UsuariosAplicacao");

            migrationBuilder.DropTable(
                name: "Condutores");

            migrationBuilder.DropTable(
                name: "Veiculos");
        }
    }
}
