﻿using AutoMapper;
using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Services.API.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Controllers
{
    [Route("api/veiculos")]
    [Authorize(Policy = "Administrador")]
    [ApiController]
    public class VeiculosController : BaseController
    {
        private readonly IVeiculoRepository _repository;
        private readonly IMapper _mapper;

        public VeiculosController(IVeiculoRepository repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("")]
        public IActionResult ObterTodos()
        {
            return Ok(_mapper.Map<IList<VeiculoDto>>( _repository.ObterTodos()));
        }

        [HttpGet("{id:guid}")]
        public IActionResult ObterPorId(Guid id)
        {
            var model = _repository.ObterPorId(id);
            if (model == null) return BadRequest("Veiculo não encontrado");

            return Ok(_mapper.Map<VeiculoDto>(model));
        }

        [HttpGet("placa/{placa}/condutores")]
        public IActionResult ObterCondutoresPorPlaca(string placa)
        {
            if (string.IsNullOrWhiteSpace(placa)) return BadRequest();

            return Ok(_mapper.Map<IList<CondutorDto>>( _repository.ObterCondutoresPorPlaca(placa)));
        }

        [HttpPost("")]
        public IActionResult Adicionar(VeiculoDto veiculoDto)
        {
            if (!veiculoDto.EstaValido()) return BadRequest(veiculoDto.ObterErros());

            veiculoDto.Id = Guid.NewGuid();

            var model = _mapper.Map<Veiculo>(veiculoDto);
             _repository.Adicionar(model);
            _repository.UnitOfWork.Commit();

            return Ok(new { isOK = "Operação realizada com sucesso" });
        }

        [HttpPut("{id:guid}")]
        public IActionResult Atualizar([FromRoute] Guid id, VeiculoDto veiculoDto)
        {
            if (veiculoDto.Id != id) return BadRequest();

            if (!veiculoDto.EstaValido()) return BadRequest(veiculoDto.ObterErros());
           
            var modelDb = _repository.ObterPorId(id);
            if (modelDb == null) return NotFound("Veículo não encontrado");

            var model = _mapper.Map<Veiculo>(veiculoDto);
            _repository.Atualizar(model);
            _repository.UnitOfWork.Commit();

            return Ok(new { isOK = "Operação realizada com sucesso" });
        }

        [HttpDelete("{id:guid}")]
        public IActionResult Remover(Guid id)
        {
            var modelDb = _repository.ObterPorId(id);
            if (modelDb == null) return NotFound("Veículo não encontrado");

             _repository.Remover(id);
             _repository.UnitOfWork.Commit();
            return Ok(new { isOK = "Operação realizada com sucesso" });
        }
    }
}
