﻿using AutoMapper;
using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Services.API.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Controllers
{
    [Route("api/condutores")]
    [Authorize(Policy = "Administrador")]
    [ApiController]
    public class CondutoresController : BaseController
    {
        private readonly ICondutorRepository _repository;
        private readonly IMapper _mapper;

        public CondutoresController(ICondutorRepository repository,
                                    IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet("")]
        public IActionResult ObterTodos()
        {
            return Ok(_mapper.Map<IList<CondutorDto>>( _repository.ObterTodos()));
        }

        [HttpGet("{id:guid}")]
        public IActionResult ObterTodos(Guid id)
        {
            var model = _repository.ObterPorId(id);
            if (model == null) return NotFound("Condutor não encontrado");

            return Ok(_mapper.Map<CondutorDto>(model));
        }

        [HttpGet("cpf/{cpf}/veiculos")]
        public IActionResult ObterVeiculosPorCpf(string cpf)
        {
            if (string.IsNullOrWhiteSpace(cpf)) return BadRequest();


            return Ok(_mapper.Map<IList<VeiculoDto>>(_repository.ObterVeiculosPorCpf(cpf)));
        }

        [HttpPost("")]
        public IActionResult Adicionar(CondutorDto condutorDto)
        {
            if (!condutorDto.EstaValido()) return BadRequest(condutorDto.ObterErros());

            condutorDto.Id = Guid.NewGuid();

            var model = _mapper.Map<Condutor>(condutorDto);
             _repository.Adicionar(model);
            _repository.UnitOfWork.Commit();

            return Ok(new { isOK = "Operação realizada com sucesso" });
        }

        [HttpPut("{id:guid}")]
        public IActionResult Atualizar([FromRoute] Guid id, CondutorDto condutorDto)
        {
            if (condutorDto.Id != id) return BadRequest();

            if (!condutorDto.EstaValido()) return BadRequest(condutorDto.ObterErros());
           
            var modelDb = _repository.ObterPorId(id);
            if (modelDb == null) return NotFound("condutor não encontrado");

            var model = _mapper.Map<Condutor>(condutorDto);
            _repository.Atualizar(model);
            _repository.UnitOfWork.Commit();

            return Ok( new { isOK = "Operação realizada com sucesso" });
        }

        [HttpDelete("{id:guid}")]
        public IActionResult Remover(Guid id)
        {
            var modelDb = _repository.ObterPorId(id);
            if (modelDb == null) return NotFound("Condutor não encontrado");

             _repository.Remover(id);
             _repository.UnitOfWork.Commit();
            return Ok(new { isOK = "Operação realizada com sucesso" });
        }

      
    }
}
