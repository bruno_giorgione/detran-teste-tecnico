﻿
using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Services.API.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Confitec.Detran.Domain.Entities;

namespace Confitec.Detran.Services.API.Controllers
{
    [Route("api/autenticacao")]
    [ApiController]
    public class AutenticacaoController : ControllerBase
    {
        private readonly IUsuarioAplicacaoRepository _repository;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;

        public AutenticacaoController(IUsuarioAplicacaoRepository repository,
                                       IMapper mapper,
                                       IConfiguration configuration)
        {
            _repository = repository;
            _mapper = mapper;
            _configuration = configuration;
        }

        [HttpPost("login")]
        public IActionResult Autenticar(UsuarioLoginDto usuarioLoginDto)
        {
            if (!usuarioLoginDto.EstaValido()) return BadRequest(usuarioLoginDto.ObterErros());

            var usuarioDb = _repository.Autenticar(usuarioLoginDto.Email, usuarioLoginDto.Senha);
            if (usuarioDb == null) return NotFound(new {key = "UserNotFound", Message = "Email e/ou senha inválidos." });

            return Ok(GerarJWTToken(usuarioDb));
        }

        private UsuarioAplicacaoAutenticadoDto GerarJWTToken(UsuarioAplicacao usuario)
        {
            var claims = new List<Claim>
            {
                  new Claim("Email", usuario.Email)
            };
            if (usuario.PermissaoTipo == PermissaoTipo.ADMIN)
            {
                claims.Add(new Claim("Role", "Administrador"));
            }

            //Gera uma chave com base em um algoritimo simetrico.
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_configuration["JwtConfig:Secret"]));

            //Gera a assinatura digital.
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);

            //Data de expiração.
            var time = int.Parse(_configuration["JwtConfig:ExpiracaoHoras"]);
            var expirationTime = DateTime.UtcNow.AddHours(time);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = _configuration["JwtConfig:Emissor"],
                Audience = _configuration["JwtConfig:ValidoEm"],
                Subject = new ClaimsIdentity(claims),
                Expires = expirationTime,
                SigningCredentials = credentials
            };

            //Gera o token
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new UsuarioAplicacaoAutenticadoDto
            {
                Token = tokenHandler.WriteToken(token),
                ExpiraEm = expirationTime,
                UsuarioData = new UsuarioData
                {
                    Nome = usuario.Nome
                }
            };
        }
    }
}
