﻿using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Dtos
{
    public class UsuarioLoginDto : DtoBase
    {
        public string Email { get; set; }
        public string Senha { get; set; }

        public bool EstaValido()
        {
            ValidationResult = new UsuarioAplicacaoDtoValidation().Validate(this);
            return ValidationResult.IsValid;
        }

        public List<ValidationFailure> ObterErros()
        {
            if (!EstaValido())
            {
                return ValidationResult.Errors;
            }
            return null;
        }

    }

    public class UsuarioAplicacaoDtoValidation : AbstractValidator<UsuarioLoginDto>
    {
        public UsuarioAplicacaoDtoValidation()
        {

            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório")
                .EmailAddress().WithMessage("{PropertyName} inválido");

            RuleFor(x => x.Senha)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");
        }
    }
}
