﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Dtos
{
    public class UsuarioAplicacaoAutenticadoDto
    {
        public string Token { get; set; }
        public DateTime ExpiraEm { get; set; }
        public UsuarioData UsuarioData { get; set; }  
    }

    public class UsuarioData
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public IEnumerable<UsuarioClaims> Claims { get; set; }
    }

    public class UsuarioClaims
    {
        public string Tipo { get; set; }
        public string Valor { get; set; }
    }
}
