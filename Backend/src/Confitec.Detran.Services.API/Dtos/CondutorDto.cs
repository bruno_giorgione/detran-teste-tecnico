﻿using Confitec.Detran.Services.API.Dtos.CustomValidations;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Dtos
{
    public class CondutorDto : DtoBase
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Telefone { get; set; }

        public string Email { get; set; }
        public string NumeroCNH { get; set; }

        public string DataNascimento { get; set; }

        public bool EstaValido()
        {
            ValidationResult = new CondutorDtoValidation().Validate(this);
            return ValidationResult.IsValid;
        }
       
        public List<ValidationFailure> ObterErros()
        {
            if (!EstaValido())
            {
                return ValidationResult.Errors;
            }
            return null;
        }
    }

    public class CondutorDtoValidation : AbstractValidator<CondutorDto>
    {
        public CondutorDtoValidation()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(x => x.CPF)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório")
                .Must(TerCpfValido).WithMessage("{PropertyName} inválido");

            RuleFor(x => x.Email)
               .NotEmpty().WithMessage("{PropertyName} é obrigatório")
               .EmailAddress().WithMessage("{PropertyName} inválido");

            RuleFor(x => x.NumeroCNH)
             .NotEmpty().WithMessage("{PropertyName} é obrigatório")
             .Length(11).WithMessage("{PropertyName} inválido.");
             

            RuleFor(x => x.DataNascimento)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório")
                .Must(SerMaiorDeIdade).WithMessage("Você precisa ter 18 anos ou mais");
        }

        protected static bool TerCpfValido(string cpf)
        {
            return CPFValidacao.IsValid(cpf);
        }
        protected static bool SerMaiorDeIdade(string dataNascimento)
        {
            if(DateTime.TryParse(dataNascimento, out DateTime dateFormatted))
            {
                return DateTime.Now.AddYears(-18).Date >= dateFormatted.Date;
            }
            return false;
         
        }
    }
}
