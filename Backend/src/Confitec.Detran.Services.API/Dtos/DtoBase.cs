﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Dtos
{
    public abstract class DtoBase
    {
        protected ValidationResult ValidationResult { get; set; }
    }
}
