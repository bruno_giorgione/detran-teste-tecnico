﻿using Confitec.Detran.Services.API.Dtos.CustomValidations;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Dtos
{
    public class VeiculoDto : DtoBase
    {
        public Guid Id { get; set; }
        public string Placa { get; set; }
        public string Modelo { get; set; }
        public string Marca { get; set; }

        public string Cor { get; set; }
        public string AnoFabricacao { get; set; }

        public bool EstaValido()
        {
            ValidationResult = new VeiculoDtoValidation().Validate(this);
            return ValidationResult.IsValid;
        }

        public List<ValidationFailure> ObterErros()
        {
            if (!EstaValido())
            {
                return ValidationResult.Errors;
            }
            return null;
        }

    }

    public class VeiculoDtoValidation : AbstractValidator<VeiculoDto>
    {
        public VeiculoDtoValidation()
        {
            RuleFor(x => x.Placa)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(x => x.Modelo)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(x => x.Marca)
               .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(x => x.Cor)
             .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(x => x.AnoFabricacao)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório.");
               

        }
    }
}
