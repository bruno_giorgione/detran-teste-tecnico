﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Configurations.Auth
{
    /// <summary>
    /// Somente usuários com perfil de administrador tem acesso.
    /// </summary>
    public static class PolicyAuthorize
    {
        public static IServiceCollection AddPolicyAuthorization(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Administrador", policy =>
                   policy.RequireClaim("Role", "Administrador"));
            });

            return services;
        }
    }
}
