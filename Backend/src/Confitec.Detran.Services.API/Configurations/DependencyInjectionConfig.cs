﻿using Confitec.Detran.Domain.Interfaces.Repositories;
using Confitec.Detran.Infrastructure.Data.SqlServer.Contexts;
using Confitec.Detran.Infrastructure.Data.SqlServer.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Confitec.Detran.Services.API.Configurations
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencyInjection(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DetranContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
            });

            //Repostories
            services.AddScoped<ICondutorRepository, CondutorRepository>();
            services.AddScoped<IVeiculoRepository, VeiculoRepository>();
            services.AddScoped<IUsuarioAplicacaoRepository, UsuarioAplicacaoRepository>();

            //Third Party
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            return services;
        }
    }
}
