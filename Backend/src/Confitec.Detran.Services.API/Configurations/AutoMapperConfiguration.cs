﻿using AutoMapper;
using Confitec.Detran.Domain.Entities;
using Confitec.Detran.Services.API.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Confitec.Detran.Services.API.Configurations
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            CreateMap<CondutorDto, Condutor>().ReverseMap();
            CreateMap<Condutor, CondutorDto>()
                .ForMember(dest => dest.DataNascimento,
                origin => origin.MapFrom(x => x.DataNascimento.ToString("dd/MM/yyyy"))).ReverseMap();

            CreateMap<VeiculoDto, Veiculo>().ReverseMap();
           
        }
    }
}
