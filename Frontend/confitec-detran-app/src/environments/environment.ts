export const environment = {
  production: false,
  apis: {
    baseurl: 'http://localhost:7192/api/',
    path: {
      login: 'autenticacao/login',
      condutor: 'condutores',
      veiculos: 'veiculos',
    }
  },
};

