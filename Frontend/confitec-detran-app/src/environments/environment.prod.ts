export const environment = {
  production: true,
  apis: {
    baseurl: 'http://localhost:7192/api/',
    path: {
      login: 'autenticacao/login',
      condutor: 'condutores',
      veiculos: 'veiculos',
    }
  },
};

