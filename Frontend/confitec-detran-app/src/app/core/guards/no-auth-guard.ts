import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { AuthUserService } from '../services/user/auth-user.service';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(
    private authUserService: AuthUserService,
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authUserService.isLogged()) {
      return true;
    }
    this.router.navigate(['/painel']);
    return false;
  }
}
