import { CanLoad, UrlSegment, Route, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { AuthUserService } from '../services/user/auth-user.service';

@Injectable()
export class AuthGuard implements CanLoad {

  constructor(
    private authUserService: AuthUserService,
    private router: Router) {}


  canLoad(route: Route, segments: UrlSegment[]): boolean | Observable<boolean> | Promise<boolean> {

    if (this.authUserService.isLogged()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;

  }

}
