import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthUserService {
  constructor(private http: HttpClient) {}

  autheticate(email: string, password: string) {
    let url = `${environment.apis.baseurl}${environment.apis.path.login}`;

    let bodyContent = {
      email: email,
      senha: password,
    };

    return this.http.post(url, bodyContent).pipe(
      map((resp: any) => {
        if (resp) {

          //armazena o token bearer.
          this.setToken(resp['token']);
        }
      })
    );
  }

  isLogged() {
    return this.hasToken();
  }

  private hasToken(){
    return !!this.getToken();
  }

   getToken() {
    return sessionStorage.getItem('token');
  }

  private setToken(token: string) {
    sessionStorage.setItem('token', token);
  }

  logout() {
    sessionStorage.clear();
  }
}
