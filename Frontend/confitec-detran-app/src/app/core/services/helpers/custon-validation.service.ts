import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidatorFn } from '@angular/forms';

@Injectable()
export class CustomValidationsService {

  constructor() { }


  isValidCPF(control: AbstractControl) {

    const cpf = control.value;
    if (cpf) {
      let numbers, digits, sum, i, result, equalDigits;
      equalDigits = 1;
      if (cpf.length < 11) {
        return { isInvalidCPF: true };
      }

      for (i = 0; i < cpf.length - 1; i++) {
        if (cpf.charAt(i) !== cpf.charAt(i + 1)) {
          equalDigits = 0;
          break;
        }
      }

      if (!equalDigits) {
        numbers = cpf.substring(0, 9);
        digits = cpf.substring(9);
        sum = 0;
        for (i = 10; i > 1; i--) {
          sum += numbers.charAt(10 - i) * i;
        }

        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

        if (result !== Number(digits.charAt(0))) {
          return { isInvalidCPF: true };
        }
        numbers = cpf.substring(0, 10);
        sum = 0;

        for (i = 11; i > 1; i--) {
          sum += numbers.charAt(11 - i) * i;
        }
        result = sum % 11 < 2 ? 0 : 11 - (sum % 11);

        if (result !== Number(digits.charAt(1))) {
          return { isInvalidCPF: true };
        }
        return null;
      } else {
        return { isInvalidCPF: true };
      }
    }
    return null;

  };



  isValidEmail(control: AbstractControl) {

    const email = control.value;
    if (email) {

      const emailRegex = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/;

      return email.match(emailRegex)
        ? null
        : { isInvalidEmail: true };
    }

    return null;
  }

  isDate18orMoreYearsOld(control: AbstractControl) {
     const date = control.value;
     if(date){
      let ptDatePattern =  /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;

      console.log(date)
      if (date.match(ptDatePattern))
        {

          if(control.valid){
            var dateFormated = null;
            dateFormated = new Date(control.value);
            if(new Date(dateFormated.getFullYear()+ 18, dateFormated.getMonth() -1, dateFormated.getDay()) > new Date()){
              return { "isUnder18Years": true };
            }
          }
     }
     else{
      return { "isInvalidDate": true };
     }

    }
    return null;
  }

  isValidBrazilPlate(control: FormControl){

    let value = control.value;
    if(value){
      let platePattern = '[A-Z]{3}[0-9][0-9A-Z][0-9]{2}';
      if (!value.match(platePattern)) {
        return { "isInvalidBrazilPlate": true };
      }
    }
    return null;
  }

  isValidNumber(control: FormControl){
    let value = control.value;
    if(value){
      let numberPattern = /^[0-9]*$/;
      if (!value.match(numberPattern)) {
        return { "isInvalidNumber": true };
      }
    }

    return null;
  }
}
