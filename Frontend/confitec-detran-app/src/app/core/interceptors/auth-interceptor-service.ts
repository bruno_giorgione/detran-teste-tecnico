import {Router} from '@angular/router';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthUserService } from '../services/user/auth-user.service';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

    constructor(private router: Router,
                private authUserService: AuthUserService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (this.authUserService.isLogged()) {
            const token = this.authUserService.getToken();
            req = req.clone({setHeaders: {Authorization: 'Bearer ' + token},});
        }

        return next.handle(req).pipe(tap(() => {
            },
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status !== 401) {
                        return;
                    }
                    this.router.navigate(['login']);
                }

            }
            )
        );
    }

}
