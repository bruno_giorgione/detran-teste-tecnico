import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from "@angular/common/http";

import { CustomValidationsService } from './services/helpers/custon-validation.service';
import { AuthUserService } from './services/user/auth-user.service';
import { interceptorProviders } from './interceptors/interceptor-providers';
import { NoAuthGuard } from './guards/no-auth-guard';
import { AuthGuard } from './guards/auth-guard';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [],
  providers:[
    AuthUserService,
    CustomValidationsService,
    NoAuthGuard,
    AuthGuard,
    interceptorProviders
  ]
})
export class CoreModule { }
