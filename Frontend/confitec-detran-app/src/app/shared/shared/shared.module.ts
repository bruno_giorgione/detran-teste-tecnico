import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessageValidationComponent } from '../components/message-validation/message-validation.component';
import { LoaderButtonDirective } from '../directives/loader-button.directive';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot(),
  ],
  declarations: [
    MessageValidationComponent,
    LoaderButtonDirective
  ],
  exports:[
    ReactiveFormsModule,
    NgxMaskModule,
    MessageValidationComponent,
    LoaderButtonDirective
  ]
})
export class SharedModule { }
