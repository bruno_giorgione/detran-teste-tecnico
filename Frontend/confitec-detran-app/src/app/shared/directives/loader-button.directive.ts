import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Directive({
  selector: '[appLoaderButton]'
})
export class LoaderButtonDirective {

  @Input() isLoading : boolean = false;
  @Input() buttonText : string = '';
  @Input() loadingText : string = 'Aguarde...';

  constructor( private renderer: Renderer2,
               private elem: ElementRef) { }

  ngOnInit(){
      this.changeAttributes(this.isLoading);
  }

  ngOnChanges(changes: SimpleChanges) {
    
      this.changeAttributes(changes.isLoading.currentValue);
  }

  private changeAttributes(isLoading: boolean){
    let button = this.buttonText;

      if(isLoading){
        button = this.loadingText;
        this.renderer.setAttribute(this.elem.nativeElement, 'disabled', 'disabled');
      }
      else{
        this.renderer.removeAttribute(this.elem.nativeElement, 'disabled');
      }

      this.renderer.setProperty(this.elem.nativeElement, 'textContent', button);
  }

}
