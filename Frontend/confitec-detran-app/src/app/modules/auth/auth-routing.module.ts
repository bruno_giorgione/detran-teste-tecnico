import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/guards/auth-guard';
import { HomeComponent } from '../home/home.component';

import { AuthComponent } from './auth.component';
import { CondutorEditarComponent } from './condutor/components/edicao/condutor-editar.component';
import { CondutorCadastrarComponent } from './condutor/components/cadastrar/condutor-cadastrar.component';
import { CondutorListagemComponent } from './condutor/components/listagem/condutor-listagem.component';
import { VeiculoListagemComponent } from './veiculo/components/listagem/veiculo-listagem.component';
import { VeiculoCadastrarComponent } from './veiculo/components/cadastrar/veiculo-cadastrar.component';
import { VeiculoEditarComponent } from './veiculo/components/edicao/veiculo-editar.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        redirectTo: 'condutores'
      },
      {
        path: 'condutores',
        component: CondutorListagemComponent
      },
      {
        path: 'condutores/cadastrar',
        component: CondutorCadastrarComponent
      },
      {
        path: 'condutores/:id/editar',
        component: CondutorEditarComponent
      },
      {
        path: 'veiculos',
        component: VeiculoListagemComponent
      },
      {
        path: 'veiculos/cadastrar',
        component: VeiculoCadastrarComponent
      },
      {
        path: 'veiculos/:id/editar',
        component: VeiculoEditarComponent
      }
    ]

  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
