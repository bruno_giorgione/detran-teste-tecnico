import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Veiculo } from '../models/veiculo.model';

@Injectable()
export class VeiculoService {

  url = `${environment.apis.baseurl}${environment.apis.path.veiculos}`;

  constructor(private http: HttpClient) {}

  obterLista(): Observable<Veiculo[]> {
    return this.http.get<Veiculo[]>(this.url);
  }

  obterPorId(id: string): Observable<Veiculo> {
    return this.http.get<Veiculo>(`${this.url}/${id}`);
  }

  cadastrar(condutor: Veiculo) {
    return this.http.post(this.url, condutor);
  }

  editar(id: string,condutor: Veiculo) {
    return this.http.put(`${this.url}/${id}`, condutor);
  }

  remover(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
