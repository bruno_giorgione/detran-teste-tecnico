import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidationsService } from 'src/app/core/services/helpers/custon-validation.service';
import { Veiculo } from '../../models/veiculo.model';
import { VeiculoService } from '../../services/veiculo.service';

@Component({
  templateUrl: './veiculo-cadastrar.component.html',
  styleUrls: ['./veiculo-cadastrar.component.scss']
})
export class VeiculoCadastrarComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private veiculoService: VeiculoService,
    private customValidationService: CustomValidationsService
  ) {}

  cadastrarForm!: FormGroup;

  isProcessing: boolean = false;
  isInvalidUser: boolean = false;
  isSuccess = false;

  ngOnInit() {
    this.cadastrarForm = this.fb.group({
      placa: ['',[Validators.required, this.customValidationService.isValidBrazilPlate]],
      modelo: ['', [Validators.required]],
      marca: ['', [Validators.required]],
      cor: ['', [Validators.required]],
      anoFabricacao: ['', [Validators.required, this.customValidationService.isValidNumber]]
    });
  }

  onSubmit(){
    if(this.cadastrarForm.valid){
      let model = this.cadastrarForm.getRawValue() as Veiculo;

      this.isProcessing = true;
      this.veiculoService.cadastrar(model)
      .subscribe(
        () => {
            this.isSuccess = true;
            this.cadastrarForm.reset();
            this.cadastrarForm.setValidators(null);
            Object.keys(this.cadastrarForm.controls).forEach(key => {

              this.cadastrarForm.get(key)?.setErrors(null) ;
            });
        },
        (err) => {
          console.log(err)
        }
      ).add(() => {
        this.isProcessing = false;
      });;


    }

  }

}
