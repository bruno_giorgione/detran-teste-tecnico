import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Veiculo } from '../../models/veiculo.model';
import { VeiculoService } from '../../services/veiculo.service';

@Component({
  templateUrl: './veiculo-listagem.component.html',
  styleUrls: ['./veiculo-listagem.component.scss']
})
export class VeiculoListagemComponent implements OnInit {

  constructor(private veiculoService: VeiculoService,
              private activatedRoute: ActivatedRoute) { }

  $veiculos!: Observable<Veiculo[]>

  ngOnInit() {
    this.obterLista();
  }

  onRemoveItem(id: string){
    if(id){
        this.veiculoService.obterPorId(id)
            .subscribe(
              data=>{
                  if(data){
                    this.veiculoService.remover(id)
                      .subscribe(
                        ()=>{
                          this.obterLista();
                        }
                      )
                  }
              }
            )
        }
  }

  private obterLista(){
    this.$veiculos = this.veiculoService.obterLista();
  }

}
