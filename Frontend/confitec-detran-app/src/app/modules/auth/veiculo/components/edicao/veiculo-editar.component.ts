import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { CustomValidationsService } from 'src/app/core/services/helpers/custon-validation.service';
import { Veiculo } from '../../models/veiculo.model';
import { VeiculoService } from '../../services/veiculo.service';

@Component({
  templateUrl: './veiculo-editar.component.html',
  styleUrls: ['./veiculo-editar.component.scss']
})
export class VeiculoEditarComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private veiculoService: VeiculoService,
    private customValidationService: CustomValidationsService
  ) {}

  editarForm!: FormGroup;
  condutorEdit!: Veiculo;
  idVeiculo: string = '';

  isProcessing: boolean = false;
  isInvalidUser: boolean = false;
  isSuccess = false;

  ngOnInit() {

    this.editarForm = this.fb.group({
      id: [''],
      placa: ['',[Validators.required, this.customValidationService.isValidBrazilPlate]],
      modelo: ['', [Validators.required]],
      marca: ['', [Validators.required]],
      cor: ['', [Validators.required]],
      anoFabricacao: ['', [Validators.required, this.customValidationService.isValidNumber]]
    });


    this.idVeiculo = this.getIdFromUrl();
    if(this.idVeiculo){
        this.veiculoService.obterPorId(this.idVeiculo)
            .subscribe(
              data=>{
                  if(data){
                    this.editarForm.patchValue(data);
                  }
                  else{
                    this.router.navigate(['../veiculos'], {relativeTo: this.activatedRoute})
                  }
              }
            )
        }
  }

  onSubmit(){
    if(this.editarForm.valid){

      let model = this.editarForm.getRawValue() as Veiculo;
      this.isProcessing = true;
      this.veiculoService.editar(this.idVeiculo,model)
      .subscribe(
        () => {

            this.isSuccess = true;
        },
        (err) => {
          console.log(err)
        }
      ).add(() => {
        this.isProcessing = false;
      });
    }
  }

  getIdFromUrl(){
    return this.activatedRoute.snapshot.params.id;
  }
}
