export class Veiculo {
  id: string = '';
  placa: string = '';
  modelo: string = '';
  marca: string = '';
  cor: string = '';
  anoFabricacao: string = '';
}
