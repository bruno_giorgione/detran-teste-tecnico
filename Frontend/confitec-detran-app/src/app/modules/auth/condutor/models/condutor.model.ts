export class Condutor {
  id: string = '';
  nome: string = '';
  cpf: string = '';
  email: string = '';
  telefone: string = '';
  numeroCNH: string = '';
  dataNascimento: string = '';
}
