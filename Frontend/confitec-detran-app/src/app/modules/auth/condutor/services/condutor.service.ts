import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Condutor } from '../models/condutor.model';

@Injectable()
export class CondutorService {
  url = `${environment.apis.baseurl}${environment.apis.path.condutor}`;

  constructor(private http: HttpClient) {}

  obterLista(): Observable<Condutor[]> {
    return this.http.get<Condutor[]>(this.url);
  }

  obterPorId(id: string): Observable<Condutor> {
    return this.http.get<Condutor>(`${this.url}/${id}`);
  }

  cadastrar(condutor: Condutor) {
    return this.http.post(this.url, condutor);
  }

  editar(id: string,condutor: Condutor) {
    return this.http.put(`${this.url}/${id}`, condutor);
  }

  remover(id: string) {
    return this.http.delete(`${this.url}/${id}`);
  }
}
