import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CustomValidationsService } from 'src/app/core/services/helpers/custon-validation.service';
import { Condutor } from '../../models/condutor.model';
import { CondutorService } from '../../services/condutor.service';

@Component({
  templateUrl: './condutor-cadastrar.component.html',
  styleUrls: ['./condutor-cadastrar.component.scss']
})
export class CondutorCadastrarComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private codutorService: CondutorService,
    private customValidationService: CustomValidationsService
  ) {}

  cadastrarForm!: FormGroup;

  isProcessing: boolean = false;
  isInvalidUser: boolean = false;
  isSuccess = false;

  ngOnInit() {
    this.cadastrarForm = this.fb.group({
      nome: ['',[Validators.required]],
      cpf: ['', [Validators.required, this.customValidationService.isValidCPF]],
      email: ['', [Validators.required, this.customValidationService.isValidEmail]],
      telefone: ['', Validators.required],
      numeroCNH: ['', [Validators.required, Validators.minLength(11)]],
      dataNascimento: ['',[Validators.required,this.customValidationService.isDate18orMoreYearsOld]],
    });
  }

  onSubmit(){
    if(this.cadastrarForm.valid){
      let model = this.cadastrarForm.getRawValue() as Condutor;

      this.isProcessing = true;
      this.codutorService.cadastrar(model)
      .subscribe(
        () => {
            this.isSuccess = true;
            this.cadastrarForm.reset();
            this.cadastrarForm.setValidators(null);
            Object.keys(this.cadastrarForm.controls).forEach(key => {

              this.cadastrarForm.get(key)?.setErrors(null) ;
            });
        },
        (err) => {
          console.log(err)
        }
      ).add(() => {
        this.isProcessing = false;
      });;


    }

  }

}
