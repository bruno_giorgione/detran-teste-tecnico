import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomValidationsService } from 'src/app/core/services/helpers/custon-validation.service';
import { Condutor } from '../../models/condutor.model';
import { CondutorService } from '../../services/condutor.service';

@Component({
  templateUrl: './condutor-editar.component.html',
  styleUrls: ['./condutor-editar.component.scss']
})
export class CondutorEditarComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private codutorService: CondutorService,
    private customValidationService: CustomValidationsService
  ) {}

  editarForm!: FormGroup;
  condutorEdit!: Condutor;
  idCondutor: string = '';

  isProcessing: boolean = false;
  isInvalidUser: boolean = false;
  isSuccess = false;

  ngOnInit() {

    this.editarForm = this.fb.group({
      id: [''],
      nome: ['',[Validators.required]],
      cpf: ['', [Validators.required, this.customValidationService.isValidCPF]],
      email: ['', [Validators.required, this.customValidationService.isValidEmail]],
      telefone: ['', Validators.required],
      numeroCNH: ['', [Validators.required, Validators.minLength(11)]],
      dataNascimento: ['', [Validators.required, this.customValidationService.isDate18orMoreYearsOld]],
    });

    this.idCondutor = this.getIdFromUrl();
    if(this.idCondutor){
        this.codutorService.obterPorId(this.idCondutor)
            .subscribe(
              data=>{
                  if(data){
                    this.editarForm.patchValue(data);
                  }
                  else{
                    this.router.navigate(['../condutores'], {relativeTo: this.activatedRoute})
                  }
              }
            )
        }
  }

  onSubmit(){

    if(this.editarForm.valid){
      let model = this.editarForm.getRawValue() as Condutor;
      this.isProcessing = true;
      this.codutorService.editar(this.idCondutor,model)
      .subscribe(
        () => {

            this.isSuccess = true;
        },
        (err) => {
          console.log(err)
        }
      ).add(() => {
        this.isProcessing = false;
      });
    }
  }

  getIdFromUrl(){
    return this.activatedRoute.snapshot.params.id;
  }
}
