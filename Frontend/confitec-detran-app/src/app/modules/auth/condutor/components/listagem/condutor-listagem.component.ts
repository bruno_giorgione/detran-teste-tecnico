import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Condutor } from '../../models/condutor.model';
import { CondutorService } from '../../services/condutor.service';

@Component({
  templateUrl: './condutor-listagem.component.html',
  styleUrls: ['./condutor-listagem.component.scss']
})
export class CondutorListagemComponent implements OnInit {

  constructor(private condutorService: CondutorService,
              private codutorService: CondutorService,) { }

  $condutores!: Observable<Condutor[]>
  isDeletedItem : boolean = false;

  ngOnInit() {
    this.obterLista();
  }

  onRemoveItem(id: string){
    if(id){
        this.codutorService.obterPorId(id)
            .subscribe(
              data=>{
                  if(data){
                    this.codutorService.remover(id)
                      .subscribe(
                        ()=>{
                          this.obterLista();
                        }
                      )
                  }
              }
            )
        }
  }

  private obterLista(){
    this.$condutores = this.condutorService.obterLista();
  }

}
