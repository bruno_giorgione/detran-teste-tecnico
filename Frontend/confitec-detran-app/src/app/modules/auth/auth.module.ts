import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from 'src/app/shared/shared/shared.module';
import { CondutorCadastrarComponent } from './condutor/components/cadastrar/condutor-cadastrar.component';
import { CondutorEditarComponent } from './condutor/components/edicao/condutor-editar.component';
import { CondutorListagemComponent } from './condutor/components/listagem/condutor-listagem.component';
import { VeiculoListagemComponent } from './veiculo/components/listagem/veiculo-listagem.component';
import { VeiculoCadastrarComponent } from './veiculo/components/cadastrar/veiculo-cadastrar.component';
import { VeiculoEditarComponent } from './veiculo/components/edicao/veiculo-editar.component';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ],
  declarations: [
    AuthComponent,
    CondutorListagemComponent,
    CondutorCadastrarComponent,
    CondutorEditarComponent,
    VeiculoListagemComponent,
    VeiculoCadastrarComponent,
    VeiculoEditarComponent
  ]
})
export class AuthModule { }
