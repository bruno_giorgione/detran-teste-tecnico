import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { AuthUserService } from 'src/app/core/services/user/auth-user.service';
import { CondutorService } from './condutor/services/condutor.service';
import { VeiculoService } from './veiculo/services/veiculo.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
  providers:[CondutorService, VeiculoService],
  encapsulation: ViewEncapsulation.None
})
export class AuthComponent implements OnInit {

  constructor(private authService: AuthUserService,
              private router: Router) { }

  ngOnInit() {

  }
  onLogout(){
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
