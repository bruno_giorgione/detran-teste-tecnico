import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { CustomValidationsService } from 'src/app/core/services/helpers/custon-validation.service';
import { AuthUserService } from 'src/app/core/services/user/auth-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private customValidationService: CustomValidationsService,
    private authService: AuthUserService
  ) {}

  loginForm!: FormGroup;

  isProcessing: boolean = false;
  isInvalidUser: boolean = false;

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: [
        '',
        [Validators.required, this.customValidationService.isValidEmail],
      ],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.isProcessing = true;
      this.isInvalidUser = false;
      const email = this.loginForm.get('email')?.value;
      const password = this.loginForm.get('password')?.value;

      this.authService.autheticate(email, password).subscribe(
        () => {
          this.router.navigate(['painel/condutores']);
        },
        (err) => {
          if (err.status == 404) {
            if (err.error.key == 'UserNotFound') {
              this.isInvalidUser = true;
              this.isProcessing = false;
            }
          }
        }
      );

    }
  }
}
