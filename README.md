### O que está composto no repositório? ###

* Aplicação backend  - ASP.NET CORE 5.0
* Aplicação frontend - ANGULAR 12

### Como rodar os projetos ###

Backend:
Abrir a solution Detran.Sln. Apos isso, executar o Update-Database do Migration, com isso será gerado as tabelas e também será criado um registro na tabela de UsuariosAplicacao(responsável por logar na aplicação).
Após isso, basta executar o projeto.

Estou utlizando o SQL SERVER no localdb, dentro do appsettings va constar o nome do banco que será gerado.

Frontend:
Abrir o diretório "Frontend\confitec-detran-app" e executar o comando npm i.
Após a criação do node_module, executar o comando ng serve

### Bibliotecas utilizadas no Backend ###

* Microsoft.EntityFrameworkCore
* Microsoft.EntityFrameworkCore.SqlServer
* Microsoft.EntityFrameworkCore.Tools
* Microsoft.AspNetCore.Mvc.Testing ( para o teste de integração)
* AutoMapper
* FluentValidation
* Microsoft.AspNetCore.Authentication.JwtBearer
* System.IdentityModel.Tokens.Jwt

### Bibliotecas utilizadas no Frontend ###

* bootstrap
* ngx-mask

### Funcionamento da arquitetura no C# ###

Foi desenvolvido uma arquitetura utilizando a abordagem do DDD, onde utilizo a Domain para centralizar as minhas entidades e interfaces dos repositórios. Na camada de Infrastructure, centralizo os respositórios, os mapeamento das entidades e as migrations.

Dentro dessa camada foi criada como uma Class Library específica  para o SqlServer, pois assim a arquitetura fica preparada para receber um outro possível banco de dados, como um MongoDB (CQRS talvez ), MySql e etc.
Não foi necessário criar a fina camada de Application, pois as próprias controllers da Web API já fazem esse papel.
As apis de Condutores e de Veículos só podem ser acessadas via Bearer token, e precisam que o usuário logado tenha  perfil de administrador.
Um Swagger com Bearer token também foi disnponilizado para testes.

E por último criei um pequeno teste de integração para a obtenção de um token, dado um usuário e senha.

**Importante** : Na migration será criado um registro com um usuário para acesso na aplicação pela angular
Email: admin@confitec.com.br
Senha: 123


### Funcionamento da arquitetura no Angular ###
Para o Agular, separei a aplicação em Shared, Core e Modules.
Shared: Vai ter todos os componentes, diretivas, pipes de forma reutilizáveis, para esse projeto somente deixei um componente de mensagem de erro, e uma diretiva para realizar um loader no botão do processamento nas telas.

Na Core, deixei o serviço singleton de autenticação e os guards para verificar se o usuário está logado ou não.

Na Modules, separei a área deslogada (Home) e a área logada (Auth), acredito que dessa forma fica mais fácil para navegar entre os componentes.
Com essa separação utilizei o Lazzy loading para carregar as rotas.

**Importante** : Dentro dos environments, estão centralizados os endpoints da aplicação do backend, como estou testando local, está fixo **http://localhost:7192**. Esta url representa a aplicação quando o ASP.NET incicia. Acredito que não mude quando forem testar, mas caso mude, basta alterar nos environments.

**Importante** : Na migration pelo C#, será criado um registro com um usuário para acesso na aplicação.
Email: admin@confitec.com.br
Senha: 123